﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Models.Validation
{
    public class ParticipantsValidator : AbstractValidator<Participants>
    {
        public ParticipantsValidator()
        {
            RuleFor(x=>x.AwayTeam).NotEmpty();
            RuleFor(x => x.HomeTeam).NotEmpty();
        }
    }
}
