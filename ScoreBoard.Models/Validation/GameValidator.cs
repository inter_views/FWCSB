﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Models.Validation
{
    public class GameValidator : AbstractValidator<Game>
    {
        public GameValidator()
        {
            RuleFor(x => x.Participants).NotEmpty();
            RuleFor(x => x.Score).NotEmpty();

            RuleFor(x => x.GameName).NotEmpty();
            RuleFor(x => x.GameEnd).GreaterThanOrEqualTo(x => x.GameStart).Unless(x=>x.GameStart == null);
            RuleFor(x => x.GameStart).LessThanOrEqualTo(x => x.GameEnd).Unless(x => x.GameEnd == null);

            RuleFor(x => x.GameIdentifier).NotEmpty();
            RuleFor(x => x.Created).NotEmpty();
        }
    }
}
