﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Models.Validation
{
    public class ScoreValidator : AbstractValidator<Score>
    {
        public ScoreValidator()
        {
            RuleFor(x=>x.AwayTeam).GreaterThanOrEqualTo(0);
            RuleFor(x => x.HomeTeam).GreaterThanOrEqualTo(0);
        }
    }
}
