﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Models
{
    public enum GameStatus
    {
        NotStarted,
        InPlay,
        Ended
    }
}
