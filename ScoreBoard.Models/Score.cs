﻿namespace ScoreBoard.Models
{
    public class Score
    {
        /// <summary>
        /// Home Team Score
        /// </summary>
        public int HomeTeam { get; set; } = 0;

        /// <summary>
        /// Away Team Score
        /// </summary>
        public int AwayTeam { get; set; } = 0;
    }
}