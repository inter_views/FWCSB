﻿namespace ScoreBoard.Models
{
    public class Game
    {
        /// <summary>
        /// Game Score
        /// </summary>
        public Score Score { get; set; }

        /// <summary>
        /// Game Participants
        /// </summary>
        public Participants Participants { get; set; }

        /// <summary>
        /// Name of the Game
        /// </summary>
        public string GameName { get; set; }

        /// <summary>
        /// Internal Game identifier
        /// </summary>
        public Guid GameIdentifier { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Date Time when record was created
        /// </summary>
        public DateTime Created { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// Indicates when the game started
        /// </summary>
        public DateTime? GameStart { get; set; }

        /// <summary>
        /// Indicates when the game ended
        /// </summary>
        public DateTime? GameEnd { get; set; }

        /// <summary>
        /// Indicates the status of the game
        /// </summary>
        public GameStatus GameStatus { get; set; }
    }
}