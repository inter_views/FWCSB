﻿namespace ScoreBoard.Models
{
    public class Participants
    {
        /// <summary>
        /// Home Team Name
        /// </summary>
        public string HomeTeam { get; set; }

        /// <summary>
        /// Away Team Name
        /// </summary>
        public string AwayTeam { get; set; }
    }
}