using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScoreBoard.Models;
using ScoreBoard.Models.Validation;
using ScoreBoard.Service;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using System.Linq;
using System.Linq.Expressions;
using System;

namespace ScoreBoard.Tests
{
    [TestClass]
    public class GameTests
    {
        private List<Game> Games { get; set; } = new List<Game>();
        private GameService gameService;
        private Fixture fixture = new Fixture();

        private List<Game> BuildGames(int no = 100, bool activeOnly = true)
        {
            fixture.Customizations.Add(new RandomNumericSequenceGenerator( 0, 20));

            var random = new System.Random();

            var list = fixture.Build<Game>()
                    .With(p=> p.GameStatus, activeOnly ? GameStatus.InPlay : (GameStatus)random.Next(0,2))
                    .With(p => p.GameStart, System.DateTime.Now.AddMinutes(random.Next(10, 240)))
                    .Without(p=>p.GameEnd)
                    .CreateMany(no);
            
            return list.ToList();
        }

        [TestInitialize]
        public void TestSetup()
        {
            gameService = new GameService(Games,
                new GameValidator(),
                new ScoreValidator(),
                new ParticipantsValidator()
                );
        }

        private Game CreateGenericGame(GameStatus status = GameStatus.NotStarted)
        {
            var game = new Game()
            {
                Participants = new Participants() { AwayTeam = "Away", HomeTeam = "Home" },
                Score = new Score() { },
                GameName = "New Game",
                GameStatus = status
            };

            return game;
        }

        [TestMethod]
        [ExpectedException(typeof(FluentValidation.ValidationException))]
        public async Task Can_Not_CreateGameAsync()
        {
            //arrange-act
            var newGame = await gameService.CreateGame(new Game()
            {
            });

            //assert
            
        }

        [TestMethod]
        [ExpectedException(typeof(FluentValidation.ValidationException))]
        public async Task Can_Not_CreateGame_Without_ParticipantsAsync()
        {
            //arrange-act
            var newGame = await gameService.CreateGame(new Game()
            {
                GameName = "Super Game",
                Score = new Score() { },
            });

            //assert
            
        }

        [TestMethod]
        [ExpectedException(typeof(FluentValidation.ValidationException))]
        public async Task Can_Not_CreateGame_Without_ScoreAsync()
        {
            //arrange-act
            var newGame = await gameService.CreateGame(new Game()
            {
                GameName = "Super Game",
                Participants = new Participants { AwayTeam="away team", HomeTeam= "home team" },
            });

            //assert

        }

        [TestMethod]
        public async Task CanCreateGameAsync()
        {
            //arrange-act
            var newGame = await gameService.CreateGame(CreateGenericGame());

            //assert
            Assert.IsTrue(newGame != null);
            Assert.IsTrue(newGame.Score.AwayTeam == 0);
            Assert.IsTrue(newGame.Score.HomeTeam == 0);
        }


        [TestMethod]
        public async Task CanStartGameAsync()
        {
            //arrange
            var newGame = await gameService.CreateGame(CreateGenericGame());

            //act
            await gameService.StartGame(newGame.GameIdentifier);

            var currentGames = await gameService.GetGames();

            //assert
            Assert.IsTrue(newGame != null);
            Assert.IsTrue(newGame.Score.AwayTeam == 0);
            Assert.IsTrue(newGame.Score.HomeTeam == 0);
            
            Assert.IsTrue(currentGames != null);
            Assert.IsTrue(currentGames.Count == 1);
            Assert.IsTrue(currentGames.First().GameName == newGame.GameName);
        }

        [TestMethod]
        public async Task CanUpdateScoreAsync()
        {
            //arrange
            var newGame = await gameService.CreateGame(CreateGenericGame());

            await gameService.StartGame(newGame.GameIdentifier);

            //act
            var gameWithNewScore = await gameService.UpdateScore(newGame.GameIdentifier, new Score { AwayTeam = 1, HomeTeam = 0 });

            //assert
            Assert.IsTrue(gameWithNewScore != null);
            Assert.IsTrue(gameWithNewScore.Score.AwayTeam == 1);
            Assert.IsTrue(gameWithNewScore.Score.HomeTeam == 0);
        }

        [TestMethod]
        [ExpectedException(typeof(FluentValidation.ValidationException))]
        public async Task Can_Not_UpdateScoreWithNegativeValue()
        {
            //arrange
            var newGame = await gameService.CreateGame(CreateGenericGame());

            //act
            var gameWithNewScore = await gameService.UpdateScore(newGame.GameIdentifier, new Score { AwayTeam = 1, HomeTeam = -5 });


            //assert
            Assert.IsTrue(gameWithNewScore != null);
            Assert.IsTrue(gameWithNewScore.Score.AwayTeam == 1);
            Assert.IsTrue(gameWithNewScore.Score.HomeTeam == -5);
        }


        [TestMethod]
        public async Task CanFinishGameAsync()
        {
            //arrange
            var game = await gameService.CreateGame(CreateGenericGame());
            await gameService.StartGame(game.GameIdentifier);

            //act
            await gameService.EndGame(game.GameIdentifier);

            //assert
            var currentGames = await gameService.GetGames();
            Assert.IsTrue(!currentGames.Contains(game));
            Assert.IsTrue(currentGames.Count() == 0);

        }

        [TestMethod]
        public async Task CanGetSummaryAsync()
        {
            //arrange
            Func<Game, bool> query = x => x.GameStatus == GameStatus.InPlay;

            var games = BuildGames(10).Where(query).Select(o => gameService.CreateGame(o).Result);

            games.ToList().ForEach(async game => await gameService.StartGame(game.GameIdentifier));

            var activeGames = await gameService.GetGames();

            var scoredGames = activeGames.Select(x => new
            {
                ScoreSum = x.Score.HomeTeam + x.Score.AwayTeam,
                HomeTeamScore = x.Score.HomeTeam,
                AwayTeamScore = x.Score.AwayTeam,
                HomeTeam = x.Participants.HomeTeam,
                AwayTeam = x.Participants.AwayTeam,
                DateCreated = x.Created
            }).OrderByDescending(x=>x.ScoreSum).ThenByDescending(x=>x.DateCreated).ToList();



            //act
            var summary = await gameService.GetSummary();


            //assert
            Assert.IsTrue(scoredGames.Any());
            Assert.IsTrue(scoredGames.Count() == summary.Count());

            var length = scoredGames.Count();

            for (int i = 0; i < length; i++)
            {
                var sc = scoredGames[i];
                Assert.IsTrue($"{sc.HomeTeam} {sc.HomeTeamScore} - {sc.AwayTeam} {sc.AwayTeamScore}" == summary.ElementAt(i));
            }

            
            
            scoredGames.ForEach(x => Console.WriteLine($"{x.HomeTeam} {x.HomeTeamScore} - {x.AwayTeam} {x.AwayTeamScore} ({x.ScoreSum}) {x.DateCreated}"));


        }
    }
}