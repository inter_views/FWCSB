﻿using ScoreBoard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Service
{
    public interface IGameService
    {
        /// <summary>
        /// Creates a new Game record
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        Task<Game> CreateGame(Game game);  
        
        /// <summary>
        /// Service to start a game
        /// </summary>
        /// <param name="gameData"></param>
        /// <returns></returns>
        Task StartGame(Guid gameId);
        
        /// <summary>
        /// Service to End the game
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        Task EndGame(Guid gameId);

        /// <summary>
        /// Service to list games
        /// </summary>
        /// <returns></returns>
        Task<IList<Game>> GetGames(bool activeOnly = true);

        /// <summary>
        /// Gets the summary
        /// </summary>
        /// <param name="activeOnly"></param>
        /// <returns></returns>
        Task<IEnumerable<string>> GetSummary(bool activeOnly = true);


        /// <summary>
        /// Update Game Scores
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="score"></param>
        /// <returns></returns>
        Task<Game> UpdateScore(Guid gameId, Score score);
    }
}
