﻿using FluentValidation;
using ScoreBoard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Service
{
    public class GameService : IGameService
    {
        private IList<Game> games { get; set; }

        private IValidator<Game> gameValidator { get; set; }
        private IValidator<Score> scoreValidator { get; set; }
        private IValidator<Participants> participantsValidator { get; set; }


        public GameService(IList<Game> games,
            IValidator<Game> gameValidator,
            IValidator<Score> scoreValidator,
            IValidator<Participants> participantsValidator
            )
        {
            this.games = games;
            this.gameValidator = gameValidator;
            this.scoreValidator = scoreValidator;
            this.participantsValidator = participantsValidator;
        }


        public async Task EndGame(Guid gameId)
        {
            var game = await FindGame(gameId);

            game.GameStatus = GameStatus.Ended;
            game.GameEnd = DateTime.UtcNow;

        }

        public async Task<IList<Game>> GetGames(bool activeOnly = true)
        {
            Func<Game, bool> all = x =>
            x.GameStatus == GameStatus.InPlay ||
            x.GameStatus == GameStatus.NotStarted;

            Func<Game, bool> active = x =>
            x.GameStatus == GameStatus.InPlay;


            return games.Where(activeOnly ? active : all).ToList();
        }

        public async Task StartGame(Guid gameId)
        {

            var game = await FindGame(gameId);

            await ValidateGame(game);

            game.GameStatus = GameStatus.InPlay;
            game.GameStart = DateTime.UtcNow;

        }

        public async Task<Game> CreateGame(Game game)
        {
            await ValidateGame(game);

            games.Add(game);

            return game;

        }

        public async Task<Game> UpdateScore(Guid gameId, Score score)
        {
            var game = await FindGame(gameId);

            await scoreValidator.ValidateAndThrowAsync(score);

            game.Score = score;

            return game;
        }



        public async Task<IEnumerable<string>> GetSummary(bool activeOnly = true)
        {
            var scoredGames = (await this.GetGames()).Select(x => new
            {
                ScoreSum = x.Score.HomeTeam + x.Score.AwayTeam,
                HomeTeamScore = x.Score.HomeTeam,
                AwayTeamScore = x.Score.AwayTeam,
                HomeTeam = x.Participants.HomeTeam,
                AwayTeam = x.Participants.AwayTeam,
                DateCreated = x.Created
            })
                .OrderByDescending(x => x.ScoreSum)
                .ThenByDescending(x => x.DateCreated)
                .ToList();

            return scoredGames.Select(x => $"{x.HomeTeam} {x.HomeTeamScore} - {x.AwayTeam} {x.AwayTeamScore}");

        }
        /// <summary>
        /// Find game by Game Id
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private Task<Game> FindGame(Guid gameId)
        {
            var game = games.Where(x => x.GameIdentifier == gameId).First();

            if (game == null) throw new Exception($"Game with ID {gameId} not found");

            return Task.FromResult(game);
        }

        /// <summary>
        /// Validate game data
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        private async Task ValidateGame(Game game)
        {
            await gameValidator.ValidateAndThrowAsync(game);
            await scoreValidator.ValidateAndThrowAsync(game.Score);
            await participantsValidator.ValidateAndThrowAsync(game.Participants);
        }

    }
}
